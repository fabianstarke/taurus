import React, { useContext, useState, useEffect } from 'react';
// eslint-disable-next-line import/no-unresolved
import PropTypes from 'prop-types';
import axios from 'axios';
import { Form, Input, Select } from 'antd';
import Button from './DefaultButton';
import { TokenContext } from '../contexts/TokenContext';

const { Option } = Select;

function TokenForm(props) {
  const { dispatch } = useContext(TokenContext);
  const [tokenName, setTokenName] = useState('');
  const [tokenTicker, setTockenTicker] = useState('');
  const [totalSupply, setTotalSupply] = useState('');
  const [issuerName, setIssuerName] = useState('');
  const [template, setTemplate] = useState('');
  const [countryList, setCountryList] = useState([]);
  const [country, setCountry] = useState('');
  const { form } = props;
  const { getFieldDecorator, validateFields } = form;

  useEffect(() => {
    axios
      .get('https://restcountries.eu/rest/v2/all')
      .then(res => {
        const countryData = res.data.map(countryName => countryName.name);
        setCountryList(countryData);
      })
      .catch(err => {
        console.log(err);
      });
  }, []);

  const handleSubmit = e => {
    e.preventDefault();
    if ((tokenName, tokenTicker, totalSupply, issuerName)) {
      dispatch({
        type: 'ADD_TOKEN',
        token: {
          tokenName,
          tokenTicker,
          totalSupply,
          issuerName,
          template,
          country,
        },
      });
      props.history.push('/');
    } else {
      validateFields();
    }
  };

  return (
    <Form onSubmit={handleSubmit} className="login-form">
      <Form.Item>
        {getFieldDecorator('tokenName', {
          rules: [{ required: true, message: 'Please input a token name!' }],
          initialValue: tokenName,
        })(
          <Input
            type="text"
            placeholder="Token Name"
            onChange={e => setTokenName(e.target.value)}
          />
        )}
      </Form.Item>
      <Form.Item>
        {getFieldDecorator('tokenTicker', {
          rules: [{ required: true, message: 'Please input a token ticker!' }],
          initialValue: tokenTicker,
        })(
          <Input
            type="text"
            placeholder="Token Ticker"
            onChange={e => setTockenTicker(e.target.value)}
          />
        )}
      </Form.Item>
      <Form.Item>
        {getFieldDecorator('totalSupply', {
          rules: [
            { required: true, message: 'Please input a total supply number!' },
          ],
          initialValue: totalSupply,
        })(
          <Input
            type="number"
            placeholder="Total Supply"
            onChange={e => setTotalSupply(e.target.value)}
          />
        )}
      </Form.Item>
      <Form.Item>
        {getFieldDecorator('issuerName', {
          rules: [{ required: true, message: 'Please input your name!' }],
          initialValue: issuerName,
        })(
          <Input
            type="text"
            placeholder="Issuer Name"
            onChange={e => setIssuerName(e.target.value)}
          />
        )}
      </Form.Item>
      <Form.Item>
        {getFieldDecorator('template', {
          rules: [{ required: true, message: 'Please input a template' }],
          initialValue: template,
        })(
          <Select onChange={e => setTemplate(e)}>
            <Option value="ERC20">ERC20</Option>
          </Select>
        )}
      </Form.Item>
      <Form.Item>
        {getFieldDecorator('country', {
          rules: [{ required: true, message: 'Please input a country' }],
          initialValue: countryList,
        })(
          <Select onChange={e => setCountry(e)}>
            {countryList.map(countrySelected => (
              <Option key={countrySelected} value={countrySelected}>
                {countrySelected}
              </Option>
            ))}
          </Select>
        )}
      </Form.Item>

      <Button
        name="Issue Token"
        background="#0df0df"
        border="#0df0df"
        color="#fff"
        htmlType="submit"
      />
    </Form>
  );
}

TokenForm.propTypes = {
  form: PropTypes.any.isRequired,
  getFieldDecorator: PropTypes.any,
  history: PropTypes.any,
};

const WrappedNormalLoginForm = Form.create({ name: 'Issue Token' })(TokenForm);

export default WrappedNormalLoginForm;
