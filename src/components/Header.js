import React from 'react';
// eslint-disable-next-line import/no-unresolved
import PropTypes from 'prop-types';
import 'antd/dist/antd.css';
import '../index.css';
import { Layout } from 'antd';

const { Header } = Layout;

export default function TaurusHeader(props) {
  const { name } = props;
  return (
    <Header className="header">
      <h1 style={{ color: '#fff' }}>{name}</h1>
    </Header>
  );
}

TaurusHeader.propTypes = {
  name: PropTypes.string.isRequired,
};
