import React, { createContext, useReducer, useEffect } from 'react';
// eslint-disable-next-line import/no-unresolved
import PropTypes from 'prop-types';
import { tokenReducer } from '../reducers/tokenReducer';

export const TokenContext = createContext();

export default function TokenContextProvider(props) {
  const [tokens, dispatch] = useReducer(tokenReducer, [], () => {
    const localData = localStorage.getItem('tokens');
    return localData ? JSON.parse(localData) : [];
  });

  useEffect(() => {
    localStorage.setItem('tokens', JSON.stringify(tokens));
  }, [tokens]);
  const { children } = props;
  return (
    <TokenContext.Provider value={{ tokens, dispatch }}>
      {children}
    </TokenContext.Provider>
  );
}

TokenContextProvider.propTypes = {
  children: PropTypes.any,
};
