import React from 'react';
// eslint-disable-next-line import/no-unresolved
import PropTypes from 'prop-types';

import { Table } from 'antd';

export default function TokenList(props) {
  const { columns, dataSource } = props;
  return (
    <Table
      columns={columns}
      pagination={{ pageSize: 10 }}
      dataSource={dataSource}
    />
  );
}

TokenList.propTypes = {
  columns: PropTypes.any.isRequired,
  dataSource: PropTypes.any.isRequired,
};
