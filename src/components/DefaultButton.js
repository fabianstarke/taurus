import React from 'react';
import PropTypes from 'prop-types';

import { Button } from 'antd';

export default function DefaultButton(props) {
  const {
    name,
    background,
    color,
    border,
    icon,
    margin,
    size = 'default',
    onClick,
    htmlType = 'submit',
  } = props;
  return (
    <Button
      size={size}
      htmlType={htmlType}
      onClick={onClick}
      style={{ background, color, border, margin }}
    >
      {name}
      {icon}
    </Button>
  );
}

DefaultButton.propTypes = {
  name: PropTypes.string.isRequired,
  size: PropTypes.string,
  background: PropTypes.string,
  border: PropTypes.string,
  color: PropTypes.string,
  icon: PropTypes.object,
  margin: PropTypes.string,
  onClick: PropTypes.any,
  htmlType: PropTypes.string,
};
