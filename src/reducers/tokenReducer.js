import uuid from 'uuid/v1';

export const tokenReducer = (state, action) => {
  switch (action.type) {
    case 'ADD_TOKEN':
      return [
        ...state,
        {
          tokenName: action.token.tokenName,
          tokenTicker: action.token.tokenTicker,
          totalSupply: action.token.totalSupply,
          issuerName: action.token.issuerName,
          template: action.token.template,
          country: action.token.country,
          key: uuid(),
        },
      ];
    case 'REMOVE_TOKEN':
      return state.filter(token => token.key !== action.key);

    default:
      return state;
  }
};
