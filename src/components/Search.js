import React from 'react';
import PropTypes from 'prop-types';
import '../index.css';
import { Input, Icon } from 'antd';

export default function Search(props) {
  const { onChange, placeholder } = props;
  return (
    <Input
      prefix={<Icon type="search" style={{ color: '#fff' }} />}
      placeholder={placeholder}
      onChange={onChange}
      className="input"
    />
  );
}

Search.propTypes = {
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
};
