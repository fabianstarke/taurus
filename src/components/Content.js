/* eslint-disable import/no-named-as-default */
/* eslint-disable import/no-named-as-default-member */
import React, { useContext, useState } from 'react';
import { Layout, Icon, Row, Col } from 'antd';
import { Link } from 'react-router-dom';
import { TokenContext } from '../contexts/TokenContext';
import Search from './Search';
import Button from './DefaultButton';
import TokenList from './TokenList';

export default function Content() {
  const { tokens, dispatch } = useContext(TokenContext);
  const [search, setSearch] = useState('');
  const columns = [
    {
      title: 'Token name',
      dataIndex: 'tokenName',
      key: 'Token name',
    },
    {
      title: 'Token ticker',
      dataIndex: 'tokenTicker',
      key: 'Token ticker',
    },
    {
      title: 'Total supply',
      dataIndex: 'totalSupply',
      key: 'Total supply',
    },

    {
      title: 'Issuer name',
      dataIndex: 'issuerName',
      key: 'Issuer name',
    },
    {
      title: 'Template',
      dataIndex: 'template',
      key: 'Template',
    },
    {
      title: 'Country',
      dataIndex: 'country',
      key: 'country',
    },
    {
      key: 'action',
      align: 'right',
      render() {
        return (
          <Icon
            onClick={() =>
              tokens.map(token =>
                dispatch({ type: 'REMOVE_TOKEN', key: token.key })
              )
            }
            type="delete"
            style={{ color: '#0df0df' }}
          />
        );
      },
    },
  ];

  const onSearch = e => {
    setSearch(e.target.value);
  };
  const filteredTokens = tokens.filter(
    token => token.tokenName.toLowerCase().indexOf(search.toLowerCase()) !== -1
  );

  return (
    <Layout
      style={{
        background: '#001529',
        padding: 24,
        margin: 0,
        minHeight: 280,
        color: '#fff',
      }}
    >
      <Row gutter={16}>
        <Col lg={14} xl={16} sm={12}>
          <Search onChange={onSearch} placeholder="Search a token name" />
        </Col>
        <Col style={{ display: 'flex' }} lg={10} xl={8} sm={12}>
          <Link to="/issueToken">
            <Button
              name="Issue Token"
              background="#0df0df"
              border="	#0df0df"
              color="#fff"
              margin="0 1rem 0 0"
            />
          </Link>
          <Button
            name="Export to CSV"
            background="#2E3F4F"
            border="2E3F4F"
            color="#fff"
            icon={<Icon type="download" style={{ color: '#fff' }} />}
          />
        </Col>
      </Row>
      <Row>
        {tokens.length ? (
          <TokenList columns={columns} dataSource={filteredTokens} />
        ) : (
          <div>Add your first token</div>
        )}
      </Row>
    </Layout>
  );
}
