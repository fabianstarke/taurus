/* eslint-disable import/no-named-as-default-member */
/* eslint-disable import/no-named-as-default */
import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import 'antd/dist/antd.css';
import { Layout } from 'antd';
import Header from './components/Header';
import SideMenu from './components/SideMenu';
import './index.css';
import './App.css';
import Content from './components/Content';
import TokenForm from './components/TokenForm';
import TokenContextProvider from './contexts/TokenContext';

function App() {
  return (
    <BrowserRouter>
      <Layout style={{ height: '100vh' }}>
        <SideMenu />
        <Layout style={{ padding: '0 24px 24px', background: '#001529' }}>
          <Header name="Token List" />
          <TokenContextProvider>
            <Switch>
              <Route exact path="/" component={Content} />
              <Route path="/issueToken" component={TokenForm} />
            </Switch>
          </TokenContextProvider>
        </Layout>
      </Layout>
    </BrowserRouter>
  );
}

export default App;
