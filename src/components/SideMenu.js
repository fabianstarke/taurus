/* eslint-disable import/no-named-as-default-member */
/* eslint-disable import/no-named-as-default */
import React from 'react';
import { Link } from 'react-router-dom';
import 'antd/dist/antd.css';
import '../index.css';
import { Layout, Menu, Icon } from 'antd';
import logo from '../logoWhite.svg';
import useWindowSize from '../hooks';

const { Sider } = Layout;

export default function SideMenu() {
  const size = useWindowSize();
  const width = size.width / 4;
  const newWidth = width < 120 ? 120 : width;

  return (
    <Sider width={newWidth} style={{ background: '#2E3F4F' }}>
      <div className="logo">
        <img src={logo} alt="logo" width="80" />
      </div>
      <Menu
        style={{ background: '#2E3F4F' }}
        theme="dark"
        mode="inline"
        defaultSelectedKeys={['2']}
      >
        <Menu.Item key="1">
          <Link to="/issueToken">
            <Icon type="plus" />
            <span>Issue Token</span>
          </Link>
        </Menu.Item>
        <Menu.Item key="2">
          <Link to="/">
            <Icon type="menu" />
            <span>Token List</span>
          </Link>
        </Menu.Item>
      </Menu>
    </Sider>
  );
}
